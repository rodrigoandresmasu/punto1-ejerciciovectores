/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Estudiante;
import Modelo.Materia;

/**
 *  Tenemos 3 materias
 * @author madarme
 */
public class SistemaInformacion {
    
    
    private Materia myMaterias[]=new Materia[3];
    private String urlProgramacionIII;
    private String urlMatematicaIII;
    private String urlFisicaII;

    public SistemaInformacion() {
    }

    /**
     * Solución del punto 1
     * @param urlProgramacionIII cadena con la url con la información de estudiantes de programación III
     * @param urlMatematicaIII cadena con la url con la información de estudiantes de matemáticas III
     * @param urlFisicaII cadena con la url con la información de estudiantes de física II
     */
    public SistemaInformacion(String urlProgramacionIII, String urlMatematicaIII, String urlFisicaII) {
        this.urlProgramacionIII = urlProgramacionIII;
        this.urlMatematicaIII = urlMatematicaIII;
        this.urlFisicaII = urlFisicaII;
        
        this.crearMateria(1, "Matemáticas III", this.urlMatematicaIII);
        this.crearMateria(2, "Fisica II", this.urlFisicaII);
        this.crearMateria(3, "Progam III", this.urlProgramacionIII);
    }
    
    /**
     * Crea un materia 
     * @param codigo un entero con el código de la materia
     * @param nombreMateria una cadena que contiene el nombre de la materia
     * @param url una cadena que contiene la URL de un archivo csv con la información de los estudiantes de esa materia
     */
    private void crearMateria(int codigo, String nombreMateria, String url)
    {
            // :)
    }
    
    /**
     * Punto 2.	Crear un vector con los estudiantes que tienen matriculado programación III, física II pero no Matemáticas III.
     * @return un listado de estudiantes
     */
    public Estudiante[] getListadoProgIII_FisicaII_NO_MatIII()
    {
    
        return null;
    }
    
    /**
     * Punto 3.	Crear un vector con los estudiantes que tienen matriculado programación III y Matemáticas III pero no física II.
     * @return 
     */
    public Estudiante[] getListadoProgIII_MatIII_NO_fisII()
    {
    
        return null;
    }
    
    /**
     * Punto 4.	Crear un vector con los estudiantes que están viendo dos de las tres materias (Cualquiera que sea).
     * @return 
     */
    public Estudiante[] getListadoVer_UNICAMENTE_dosMaterias()
    {
    
        return null;
    }
    
}
